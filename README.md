## Installation

Clone this repo with git in terminnal or download directly
```bash
git clone https://gitlab.com/prdnme/pubsubemis.git 
```

Go to your directory then install the packages with composer
```bash
composer install OR composer update
```

## Using pubsub

Package yang digunakan adalah [google-cloud-php-pubsub](https://github.com/googleapis/google-cloud-php-pubsub)

Untuk melakukan <b>publish</b> data bisa menggunakan perintah artisan yang dibuat secara custom
```bash
php artisan dashboard:publish
```
Untuk mengetahui apakah data terpublish atau tidak bisa dicek melalui log laravel pada directory
<b>storage/logs/laravel.log</b> dan hasilnya seperti berikut ini
```bash
[2021-12-17 07:36:00] local.INFO: {"messageIds":["3738640890597315"]}
```

Untuk melakukan <b>subscribe</b> data dari hasil publish yaitu dengan perintah custom artisan sama seperti saat publish
```bash
php artisan dashboard:subscribe
```
Di log akan terdapat contoh data hasil dari pull berdasarkan topic yang ditentukan
```bash
[2021-12-17 07:45:40] local.INFO: data subscribe : {"data subscribe":["sample data"]}
```

Untuk semua command/perintah yang dibuat secara custom tersedia pada directory [app/Console/Commands](https://gitlab.com/prdnme/pubsubemis/-/tree/master/app/Console/Commands)
