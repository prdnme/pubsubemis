<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;

class InstitutionController extends Controller
{
    public function pubsub(Request $request){
        try {
            Log::info('processing institution test');

            $url = 'https://alter1-pendidikan.kemenag.go.id/v1/reports/createAndUpdate';
            $response = Http::post($url, $request->all());

            if ($response->failed()) {
                Log::error('ERROR '.$response->body());
                $response->throw();
            }

            Log::info($response->body());

            return $response->body();
        } catch (\Exception $e) {
            Log::error($e);
            return $e;
        }
    }
}