<?php

namespace App\Services\Institution;

use EMIS\Fragments\Models\User as EmisUser;

interface Contract
{
    public function publishDashboard();
}