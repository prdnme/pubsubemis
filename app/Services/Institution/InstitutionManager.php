<?php

namespace App\Services\Institution;

use Illuminate\Support\Manager;

class InstitutionManager extends Manager
{
    protected $app;

    public function getDefaultDriver()
    {
        return 'Institution';
    }

    public function createInstitutionDriver(): Contract
    {
        return new InstitutionService();
    }
}
