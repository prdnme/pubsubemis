<?php

namespace App\Services\Institution;

use App\Publisher\DashboardPublish;
use Illuminate\Support\Facades\Log;


class InstitutionService implements Contract
{
    public function publishDashboard()
    {
        try {
            $dashboard = new DashboardPublish;
            $val = [];
            $datas = [];
            
            $capacity = 0;
            
            $val['tahun_ajaran'] = 2021;
            $val['semester'] = 'Ganjil';
            $val['provinsi'] = 'DKI Jakarta';
            $val['kota'] = 'Jakarta Pusat';
            $val['jenjang'] = 'MA';
            $val['status_lembaga'] = 'Swasta';
            $val['madrasah'] = 'MAS Imam Annawawi';
            $val['madrasah_id'] = 1;
            $val['jumlah_gtk'] = 100;
            $val['jumlah_siswa'] = 100;
            $val['daya_tampung'] = $capacity;
            $val['jumlah_rombel'] = 100;
            $val['longitude'] = -6.164522;
            $val['latitude'] = 106.8768418;
            
            $datas[] = $val;

            $publish = [
                'datasetId' => 'publish',
                'datasetOwner' => 'emis',
                'tables' => [
                    'tableName' => 'mart_summ_public_test_2',
                    'columns' => [
                        [
                            "columnName" => "tahun_ajaran",
                            "columnType" => "STRING",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "semester",
                            "columnType" => "STRING",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "provinsi",
                            "columnType" => "STRING",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "kota",
                            "columnType" => "STRING",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "jenjang",
                            "columnType" => "STRING",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "status_lembaga",
                            "columnType" => "STRING",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "madrasah",
                            "columnType" => "STRING",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "madrasah_id",
                            "columnType" => "INT64",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "jumlah_gtk",
                            "columnType" => "INT64",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "jumlah_siswa",
                            "columnType" => "INT64",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "daya_tampung",
                            "columnType" => "INT64",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "jumlah_rombel",
                            "columnType" => "INT64",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "longitude",
                            "columnType" => "STRING",
                            "columnModel" => "NULLABLE"
                        ],
                        [
                            "columnName" => "latitude",
                            "columnType" => "STRING",
                            "columnModel" => "NULLABLE"
                        ],
                    ],
                    'data' => $datas,
                ],
            ];

            return $dashboard->pubsub(json_encode($publish));
        } catch (\Exception $e) {
            Log::info("Oppss, something error : {$e->getMessage()}");
        }
    }
}
