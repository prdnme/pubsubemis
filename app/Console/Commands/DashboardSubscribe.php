<?php

namespace App\Console\Commands;

use App\Subscribers\DashboardSubscribe as Subscribe;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;

class DashboardSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dashboard:subscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe topic dashboard-main';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $dashboard;
    public function __construct(Subscribe $dashboard)
    {
        parent::__construct();
        $this->dashboard = $dashboard;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $result = $this->dashboard->subscribe('dashboard-main-subscriber-DEVELOPMENT');
        // Log::info($result);
        foreach ($result as $value) {
            $results[] = json_decode($value->data(),true);
        }
        Log::info("data subscribe : " . print_r($results, true));
        echo 'success subscribe dashboard';
    }
}
