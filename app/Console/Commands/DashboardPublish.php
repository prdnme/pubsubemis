<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Institution\InstitutionService;
use Illuminate\Support\Facades\Log;

class DashboardPublish extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dashboard:publish {--chunk=500}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'publish data to dashboard';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $institutionData;

    public function __construct(InstitutionService $institutionData)
    {
        parent::__construct();
        $this->institution = $institutionData;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info($this->institution->publishDashboard());
        echo 'success publish data to dashboard';
    }
}