<?php

namespace App\Subscribers;

use Google\Cloud\Core\Exception\GoogleException;
use Illuminate\Support\Facades\Log;
use Google\Cloud\PubSub\PubSubClient;

class DashboardSubscribe
{
    public function subscribe($subscribe)
    {
        try {
            $dashboard = new PubSubClient([
                'projectId' => 'bi-dashboard-dev',
                'keyFilePath' => base_path('bi-dashboard-dev-15d80978dfe1.json'),
            ]);
            $topic = $dashboard->topic('dashboard-main');
            return $topic->subscription($subscribe)->pull();
        } catch (GoogleException $e) {
            Log::error($e);
            return $e;
        } catch(\Exception $e) {
            Log::error($e);
            return $e;
        }
    }
}