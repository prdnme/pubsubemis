<?php

namespace App\Subscribers;

// use App\Models\Institution;
use GDGTangier\PubSub\Subscriber\SubscriberJob;
use GDGTangier\PubSub\Subscriber\Traits\JobHandler;
use Illuminate\Support\Facades\Log;

class InstitutionSubscriber
{
    use JobHandler;

    /**
     * @var mixed
     */
    public $payload;

    /**
     * @var \GDGTangier\PubSub\Subscriber\SubscriberJob
     */
    public $job;

    /**
     * foobar constructor.
     *
     * @param \GDGTangier\PubSub\Subscriber\SubscriberJob $job
     * @param $payload
     */
    public function __construct(SubscriberJob $job, $payload)
    {
        $this->job = $job;
        $this->payload = $payload;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info($this->job, $this->payload);
        $payload = json_decode($this->payload, true);
        $institutionID = $payload['id'] ?? null;

        if (!$institutionID) {
            Log::error('invalid institution id');
            return;
        }

        
        /* Institution::updateOrInsert(
            ['id' => $institutionID],
            [
                'statistic_type' => $payload['statistic_type'] ?? '',
                'statistic_num' => $payload['statistic_num'] ?? '',
                'npsn' => $payload['npsn'] ?? '',
                'name' => $payload['name'] ?? '',
                'status' => $payload['status'] ?? 'negeri',
                'acronym' => $payload['acronym'] ?? '',
                'category_id' => $payload['institution_category']['id'] ?? null,
                'category_name' => $payload['institution_category']['name'] ?? null,
                'category_acronym' => $payload['institution_category']['acronym'] ?? null,
                'area_id' => $payload['institution_area']['id'] ?? null,
                'area_name' => $payload['institution_area']['name'] ?? null,
                'area_acronym' => $payload['institution_area']['acronym'] ?? null,
                'address' => $payload['location']['address'] ?? '',
                'province_id' => $payload['location']['province_id'] ?? null,
                'city_id' => $payload['location']['city_id'] ?? null,
                'district_id' => $payload['location']['district_id'] ?? null,
                'sub_district_id' => $payload['location']['sub_district_id'] ?? null,
                'postal_code' => $payload['location']['postal_code'] ?? null,
                'latitude' => $payload['location']['latitude'] ?? null,
                'longitude' => $payload['location']['longitude'] ?? null,
                'updated_at' => now(),
            ]
        ); */
        Log::info('processing completed');
    }
}
