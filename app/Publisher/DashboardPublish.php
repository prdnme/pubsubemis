<?php

namespace App\Publisher;

use GDGTangier\PubSub\Publisher\Facades\PublisherFacade;
use Google\Cloud\Core\Exception\GoogleException;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Google\Cloud\PubSub\PubSubClient;

class DashboardPublish
{
    public function pubsub($institution_data)
    {
        try {
            // dd(json_decode(file_get_contents(base_path('bi-dashboard-dev-15d80978dfe1.json')), true));
            $dashboard = new PubSubClient([
                'projectId' => 'bi-dashboard-dev',
                'keyFilePath' => base_path('bi-dashboard-dev-15d80978dfe1.json'),
            ]);
            $topic = $dashboard->topic('dashboard-main');
            $results = $topic->publish(['data' => $institution_data]);
            return json_encode($results);
        } catch (GoogleException $e) {
            Log::error($e);
        } catch(\Exception $e) {
            Log::error($e);
        }
    }
}