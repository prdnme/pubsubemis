<?php

return [
    'enabled' => env('PUBSUB_ENABLED', true),

    /*
     * GCP Credentials.
     */
    'credentials' => [
        'keyFilePath' => env('PUBSUB_CLIENT_KEY', 'client'),
        'keyFilePathDashboard' => env('PUBSUB_CLIENT_KEY_DASHBOARD', 'client'),
        'projectId'   => env('GCP_PROJECT_ID', 'emulator-project'),
        'projectIdDashboard'   => env('GCP_PROJECT_ID_DASHBOARD', 'emulator-project'),
    ],

    /*
     * Here where you map events name with Google Pub/Sub topics.
     *
     * Means, map each event name to specific Google Pub/Sub topic.
     */
    'events' => [
        'sample-event'   => 'dashboard-main',
        'dashboard-main'   => env('PUBSUB_TOPIC', 'institution'),
    ],

    /*
     * Here where you can tie the subscription classes (jobs) to topics.
     *
     * Means, map each subscription job to a specific Google pubsub topic.
     * The subscription job is responsible for handling the incoming data
     * from a Google Pub/Sub topic.
     */
    'subscriptions' => [
        \App\Subscribers\InstitutionSubscriber::class => 'dashboard-main',
    ],

    'emulator' => env('PUBSUB_IS_EMULATOR', false),
];